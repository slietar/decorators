/**
 *
 * decorators
 * TypeScript definition
 *
 **/


/// <reference path='./src/local-refs/bunyan/bunyan.d.ts' />


declare module '@slietar/decorators' {
  import { Logger as BunyanLogger } from 'bunyan';

  export type Logger = BunyanLogger | Console;
  export type LoggerFinder = () => Logger;

  export type AnyDecorator = ClassDecorator & MethodDecorator & ParameterDecorator & PropertyDecorator;

  export function defaults<D>(defaultValues: D, options?: { recursive?: boolean }): ParameterDecorator;

  export function deprecate(message?: string, logger?: Logger): AnyDecorator;
  export function deprecate(message?: string, logger?: LoggerFinder): AnyDecorator;

  export function once(message?: string, logger?: Logger): ClassDecorator & MethodDecorator & PropertyDecorator;
  export function once(message?: string, logger?: LoggerFinder): ClassDecorator & MethodDecorator & PropertyDecorator;
}
