/**
 *
 * decorators
 * defaults
 *
 **/


import { Decorator, TargetKind } from '@slietar/decorator';


export default Decorator({
  [TargetKind.Parameter]: function defaults(defaultObj, options) {
    let signature = this.signature;

    let defaultsObj = (obj = {}, defObj = defaultObj) => {
      if (typeof defObj !== 'object') {
        throw new Error(`DEFAULTS ${signature}: Parameter must be an object`);
      }

      for (let prop in defObj) {
        let defValue = defObj[prop];

        if  (!obj.hasOwnProperty(prop)) {
          obj[prop] = defValue;
        } else if (options && options.recursive && typeof defValue === 'object' && defValue.constructor === Object) {
          obj[prop] = defaultsObj(obj[prop], defValue);
        }
      }

      return obj;
    }

    this.cover(defaultsObj);
  }
});
