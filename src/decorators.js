/**
 *
 * decorators
 *
 **/


export { default as defaults } from './defaults';
export { default as deprecate } from './deprecate';
export { default as once } from './once';               /* experimental */
