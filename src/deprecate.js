/**
 *
 * decorators
 * deprecate
 *
 **/


import { CombinedDecorator, TargetKind } from '@slietar/decorator';


export const defaultMessage = 'This feature is deprecated and will be removed in future versions';
export const defaultLoggerFinder = function _loggerFinder() { return (this && this.logger) || global.console; };

export default CombinedDecorator(function deprecate(message = defaultMessage, logger = defaultLoggerFinder) {
  let kind = this.kind;
  let warning = `DEPRECATION ${this.signature}: ${message}`;

  if (kind !== TargetKind.Property) {
    this.cover(function (arg) {
      if (kind === TargetKind.Parameter && arg === void 0) {
        return;
      }

      logger = (typeof logger === 'function' && logger.call(this)) || logger;
      logger.warn(warning);
    });
  } else {
    this.wrap(function () {
      logger = (typeof logger === 'function' && logger.call(this)) || logger;
      logger.warn(warning);
    });
  }
});

/*export default decorate(function deprecate(message: string = 'This feature will be removed in future versions', logger: Logger | Console = console) {
  if (this.kind !== TargetKind.Class && this.kind !== TargetKind.Method) {
    throw new Error(`Cannot deprecate target of kind ${TargetKind[this.kind]}`);
  }

  const warning = `DEPRECATION ${this.signature}: ${message}`;

  if (this.kind === TargetKind.Class) {
    return (...args: any[]) => {
      logger.warn(warning);

      return classApply(this.target, ...args);
    };
  } else if (this.kind === TargetKind.Method) {
    let originalDescriptorValue = this.descriptor.value;

    this.descriptor.value = function (...args: any[]) {
      logger.warn(warning);

      return originalDescriptorValue.call(this, ...args);
    };

    return this.descriptor;
  } /* else if (this.kind === TargetKind.Property) {
    Object.defineProperty(this.target, this.propertyKey, {
      get: function () {
        console.warn(warning);
      }
    });
  } else {
    let descriptor = Object.getOwnPropertyDescriptor(this.target, this.propertyKey);
    let originalDescriptorValue = descriptor.value;
    let parameterIndex = this.parameterIndex;

    descriptor.value = function (...args: any[]) {
      if (typeof args[parameterIndex] !== typeof void 0) {
        console.warn(warning);
      }

      return originalDescriptorValue.call(this, ...args);
    };

    Object.defineProperty(this.target, this.propertyKey, descriptor);
  }
});*/
