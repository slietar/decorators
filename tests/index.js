/**
 *
 * decorators
 * tests master
 *
 **/


var fs = require('fs');
var _d = require('@slietar/decorate');

global.__decorate = _d.__decorate;
global.__param    = _d.__param;

require('babel/polyfill');


var prefix = __dirname + '/lib/';

fs.readdirSync(prefix).forEach(function (file) {
  require(prefix + file);
});
