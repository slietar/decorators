/**
 *
 * decorators
 * deprecate test
 *
 **/

/// <reference path='../../src/local-refs/chai/chai.d.ts' />
/// <reference path='../../src/local-refs/chai/chai.d.ts' />
/// <reference path='../../src/local-refs/mocha/mocha.d.ts' />
/// <reference path='../../src/local-refs/node/node.d.ts' />

import { expect } from 'chai';

import { default as deprecate, defaultMessage } from '../../lib/deprecate';


describe('@deprecate()', () => {
  it('should show a deprecation message', (done) => {
    let msg = 'foo bar';
    let i = 0;
    let logger = <Console> {
      warn: (message: string) => {
        expect(message).to.match(new RegExp(`${msg}|${defaultMessage}`));

        if (++i >= 6) {
          done();
        }
      }
    };

    let oldWarn = global.console.warn;
    global.console.warn = logger.warn;

    let D = deprecate(msg, logger);
    let E = deprecate(msg);
    let F = deprecate();

    @D
    class A {
      @D
      m(@D a: string, @E b?: string) {}

      @D
      p: string;

      @E
      q: string;

      logger: any = logger;
    }

    @F
    class B {

    }

    let a = new A();
    a.m('baz');
    a.p;
    a.q;

    let b = new B();

    global.console.warn = oldWarn;
  });
});
