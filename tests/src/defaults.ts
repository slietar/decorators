/**
 *
 * decorators
 * defaults test
 *
 **/


/// <reference path='../../src/local-refs/chai/chai.d.ts' />
/// <reference path='../../src/local-refs/mocha/mocha.d.ts' />

import { expect } from 'chai';

import defaults from '../../lib/defaults';


describe('@defaults()', () => {
  it('should fill missing values with given default values', () => {
    class A {
      m(@defaults({ a: 'a', b: 35 }) opts: Object) {
        return opts;
      }
    }

    expect(new A().m({ a: 'a', c: ['ca'] })).to.eql({ a: 'a', b: 35, c: ['ca'] });
  });

  it('should recurisvely do it with normal objects if the `recursive` option equals true', () => {
    class A {}

    let instanceA = new A();

    class B {
      m(@defaults({ a: 'a', b: instanceA, c: { ca: 45, cb: 'cb' } }, { recursive: true }) opts: Object) {
        return opts;
      }
    }

    expect(new B().m({ c: { ca: 32 } })).to.eql({ a: 'a', b: instanceA, c: { ca: 32, cb: 'cb' } });
  });

  it('should throw an error if given parameter is not an object', () => {
    class A {
      m(@defaults('foo') a: string) {}
    }

    expect(() => new A().m('foo')).to.throw(/must be an object/);
  });
});
